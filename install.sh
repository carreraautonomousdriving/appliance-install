#!/bin/bash

#configure locals
sudo echo "Configuring locals"
export LANGUAGE=en_GB.UTF-8
export LC_ALL=en_GB.UTF-8
sudo locale-gen en_GB.UTF-8
sudo dpkg-reconfigure locales  --frontend=noninteractive

#configure wifi country
sudo iw reg set DE
#remove softblock from wifi interface
rfkill unblock 0


#install necessary software
echo "Installing required software"
sudo apt-get update
sudo apt-get install dnsmasq hostapd python-dev python-rpi.gpio python3-pip python-pigpio --yes #python3-rpi.gpio 
sudo pip3 install django
sudo pip3 install RPi.GPIO



#enable hw-pwm service
sudo systemctl enable pigpiod



#configure webapp
sudo git clone https://gitlab.com/carreraautonomousdriving/webapp.git /opt/arerrac/frontend/

sudo echo "[Unit]" > /lib/systemd/system/arerrac-web.service
sudo echo "Description=Arerrac WebService" >> /lib/systemd/system/arerrac-web.service
sudo echo "After=network.target" >> /lib/systemd/system/arerrac-web.service
sudo echo "StartLimitIntervalSec=0" >> /lib/systemd/system/arerrac-web.service
sudo echo "" >> /lib/systemd/system/arerrac-web.service
sudo echo "[Service]" >> /lib/systemd/system/arerrac-web.service
sudo echo "Type=simple" >> /lib/systemd/system/arerrac-web.service
sudo echo "Restart=always" >> /lib/systemd/system/arerrac-web.service
sudo echo "RestartSec=1" >> /lib/systemd/system/arerrac-web.service
sudo echo "User=root" >> /lib/systemd/system/arerrac-web.service
sudo echo "ExecStart=/usr/bin/python3 /opt/arerrac/frontend/webserver/manage.py runserver 0.0.0.0:80" >> /lib/systemd/system/arerrac-web.service
sudo echo "" >> /lib/systemd/system/arerrac-web.service
sudo echo "[Install]" >> /lib/systemd/system/arerrac-web.service
sudo echo "WantedBy=multi-user.target" >> /lib/systemd/system/arerrac-web.service



#configure track-controller
sudo git clone https://gitlab.com/carreraautonomousdriving/backend-service.git /opt/arerrac/backend/
sudo echo "[Unit]" > /lib/systemd/system/arerrac-track.service
sudo echo "Description=Arerrac TrackService" >> /lib/systemd/system/arerrac-track.service
sudo echo "After=network.target" >> /lib/systemd/system/arerrac-track.service
sudo echo "StartLimitIntervalSec=0" >> /lib/systemd/system/arerrac-track.service
sudo echo "" >> /lib/systemd/system/arerrac-track.service
sudo echo "[Service]" >> /lib/systemd/system/arerrac-track.service
sudo echo "Type=simple" >> /lib/systemd/system/arerrac-track.service
sudo echo "Restart=always" >> /lib/systemd/system/arerrac-track.service
sudo echo "RestartSec=1" >> /lib/systemd/system/arerrac-track.service
sudo echo "User=root" >> /lib/systemd/system/arerrac-track.service
sudo echo "ExecStart=/usr/bin/python3 /opt/arerrac/backend/__init__.py" >> /lib/systemd/system/arerrac-track.service
sudo echo "" >> /lib/systemd/system/arerrac-track.service
sudo echo "[Install]" >> /lib/systemd/system/arerrac-track.service
sudo echo "WantedBy=multi-user.target" >> /lib/systemd/system/arerrac-track.service



#enable arerrac-services
sudo systemctl daemon-reload
sudo systemctl enable arerrac-web
sudo systemctl start arerrac-web
sudo systemctl enable arerrac-track
sudo systemctl start arerrac-track



#configure static local ip-address:
sudo echo "### Configuring wifi interface ###"
sudo echo "interface wlan0" >> /etc/dhcpcd.conf
sudo echo "	static ip_address=192.168.96.1/24" >> /etc/dhcpcd.conf
sudo echo "	nohook wpa_supplicant" >> /etc/dhcpcd.conf

sudo systemctl restart dhcpcd



#configure dhcp and dns server
echo "### Configuring dhcp and dns server ###"
sudo mv /etc/dnsmasq.conf /etc/backup_dnsmasq.conf
sudo echo "interface=wlan0" > /etc/dnsmasq.conf
sudo echo "dhcp-range=192.168.96.10,192.168.96.250,255.255.255.0,24h" >> /etc/dnsmasq.conf
sudo echo "" >> /etc/dnsmasq.conf
sudo echo "listen-address=127.0.0.1" >> /etc/dnsmasq.conf
sudo echo "port=53" >> /etc/dnsmasq.conf
sudo echo "no-resolv" >> /etc/dnsmasq.conf
sudo echo "" >> /etc/dnsmasq.conf
sudo echo "address=/#/192.168.96.1" >> /etc/dnsmasq.conf

sudo systemctl restart dnsmasq



#configure hostapd
sudo echo "### Configuring access point software ###"
sudo echo "interface=wlan0" > /etc/hostapd/hostapd.conf
sudo echo "driver=nl80211" >> /etc/hostapd/hostapd.conf
sudo echo "ssid=Arerrac MI" >> /etc/hostapd/hostapd.conf
sudo echo "hw_mode=g" >> /etc/hostapd/hostapd.conf
sudo echo "channel=12" >> /etc/hostapd/hostapd.conf
sudo echo "wmm_enabled=0" >> /etc/hostapd/hostapd.conf
sudo echo "macaddr_acl=0" >> /etc/hostapd/hostapd.conf
sudo echo "auth_algs=1" >> /etc/hostapd/hostapd.conf
sudo echo "ignore_broadcast_ssid=0" >> /etc/hostapd/hostapd.conf
sudo echo "wpa=2" >> /etc/hostapd/hostapd.conf
sudo echo "wpa_passphrase=arerracmi" >> /etc/hostapd/hostapd.conf
sudo echo "wpa_key_mgmt=WPA-PSK" >> /etc/hostapd/hostapd.conf
sudo echo "wpa_pairwise=TKIP" >> /etc/hostapd/hostapd.conf
sudo echo "rsn_pairwise=CCMP" >> /etc/hostapd/hostapd.conf

sudo echo "DAEMON_CONF=\"/etc/hostapd/hostapd.conf\"" >> /etc/default/hostapd

sudo systemctl unmask hostapd
sudo systemctl enable hostapd
sudo systemctl start hostapd



#status output

printf "Status hostapd: "
systemctl is-active hostapd
printf "Status dnsmasq: "
systemctl is-active dnsmasq
printf "Status dhcpcd: "
systemctl is-active dhcpcd
printf "Status arerrac-webservice: "
systemctl is-active arerrac-web
printf "Status arerrac-trackservice: "
systemctl is-active arerrac-track

sleep 5
sudo reboot now
